*** Settings ***
Resource    recursos.robot

*** Test Cases ***
G001 Búsqueda de palabras en google
    Abrir navegador y esperar logo
    Input text      xpath=//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input    ${PBuscar}
    Click Element   xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be     ${PBuscar} - Buscar con Google
    Page Should Contain     ${PBuscar}
    Close Browser

G002 Hacer click en el botón de búsqueda sin escribir palabras en Google
    Abrir navegador y esperar logo
    Click Element   xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be     Google
    Close Browser

    esto es una prueba